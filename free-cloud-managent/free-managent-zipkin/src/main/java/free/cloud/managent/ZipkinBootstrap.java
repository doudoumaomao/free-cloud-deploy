package free.cloud.managent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import zipkin.server.internal.EnableZipkinServer;

/**
 * 链路追踪服务
 * @Author: maomao
 * @Date: 2019-09-29 11:12
 */
@EnableZipkinServer
@SpringBootApplication
public class ZipkinBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinBootstrap.class, args);
    }
}
