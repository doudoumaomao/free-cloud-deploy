# Free-Cloud-Deploy

#### 介绍
本项目用于运维、服务监控、个人相关服务，没有业务代码

-------------------------------------------------------------------------------


#### 项目结构

```
free-cloud-deploy
├── develop                       -- 开发环境
|    ├── config                   -- 开发环境配置
|    ├── docker                   -- docker-compose 服务配置
|    ├── k3s                      -- k3s各环境
|    		├── dev-app             -- 一个开发应用所需的独立服务
|    		├── dev-service         -- 开发所需公共模块（如：mysql\redis\rabbitmq\elasticsearch等等）
|    		├── docker         	    -- 暂时放Rancher
|    		├── ingress-nginx       -- ingress-nginx基础服务（反向代理）
|    		├── kube-public         -- 临时测试应用
|    		├── kube-system         -- kube基础编排
|    		├── loghorn-system      -- 分布式存储（单机暂时可不用）
|    		├── tools      	        -- 工具相关（nextcloud<个人云盘>、cloud-torrent（文件下载））
├── free-cloud-getway             -- 网关
|    ├── free-getway              -- getway
├── free-cloud-managent           -- 管理监控
|    ├── free-managent-admin      -- spring boot admin service应用监控

```

-------------------------------------------------------------------------------


#### 开发环境
技术 | 名称 | 官网
---|---|---
jdk8   | java版本 |
Mysql  | 数据库 |
Nexus3 | 私有仓库 | https://repository.apache.org/
Gitee | 代码仓库 | https://gitee.com/
jenkins | CI\CD | https://jenkins-zh.cn/
Docker | 容器 | https://www.docker.com/
Nginx  | 反向代理 | http://nginx.org/
RabbitMQ | RabbitMQ | https://www.rabbitmq.com/

-------------------------------------------------------------------------------


#### 后端技术

技术 | 名称 | 官网
---|---|---
Spring Cloud gateway | 网关 |
Spring Boot Admin | 应用监控 |
grafana | 可视化监控指标展示工具 | https://grafana.com/
zipkin service| 链路追踪 | https://zipkin.io
-------------------------------------------------------------------------------


#### 安装教程

k3s安装：https://my.oschina.net/u/1019754/blog/3207875



#### 后续计划

1. 增加流量转发&流量离线转存


