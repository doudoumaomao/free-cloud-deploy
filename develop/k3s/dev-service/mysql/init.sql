## 创建nacos用户
CREATE USER 'nacos'@'%' IDENTIFIED BY 'nacos';
## 创建数据库
CREATE DATABASE nacos_devtest;
## 授权
GRANT ALL ON nacos_devtest.* TO 'nacos'@'%';






## 创建freecloud用户
CREATE USER 'freecloud'@'%' IDENTIFIED BY 'freecloud';
## 创建数据库
CREATE DATABASE free_dev;
## 授权
GRANT ALL ON free_dev.* TO 'freecloud'@'%';




## 创建owncloud用户
CREATE USER 'owncloud'@'%' IDENTIFIED BY 'owncloud';
## 创建数据库
CREATE DATABASE owncloud;
## 授权
GRANT ALL ON owncloud.* TO 'owncloud'@'%';


## 增加Prometheus mysql监控
CREATE USER 'mysql-exporter'@'%' IDENTIFIED BY 'mysql-exporter';
GRANT PROCESS, REPLICATION CLIENT ON *.* TO 'mysql-exporter'@'%';
GRANT SELECT ON performance_schema.* TO 'mysql-exporter'@'%';




