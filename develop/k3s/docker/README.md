中国大陆境内Docker镜像加速器。

阿里云：

1. 打开https://cr.console.aliyun.com，并登录自己的阿里云账户
2. 点击左方的 镜像中心 - 镜像加速器 ，然后在下方会有一个操作文档，复制其中的命令，粘贴到你的SSH窗口中即可完成镜像源的更换。

### rancher

1. 使用docker-compose.yml文件
2. 手动直接启动
```
docker run -d -v /www/k3s/data/rancher/:/var/lib/rancher/ --restart=unless-stopped --name rancher2 -p 9442:80 -p 9443:443 rancher/rancher:v2.4.2
```


### kubesphere 
