#!/bin/bash

VER=8
DIR=$(dirname $0)

set -e
docker build --pull -f $DIR/Dockerfile.openjdk.jdk $DIR -t registry.cn-beijing.aliyuncs.com/free-cloud/jdk:$VER
docker push registry.cn-beijing.aliyuncs.com/free-cloud/jdk:${VER}