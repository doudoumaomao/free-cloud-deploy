## 带字体镜像

FROM openjdk:8

MAINTAINER maomao <138383877@qq.com>

LABEL name="free-cloud/openjdk"

ENV JAVA_HOME=/usr/local/openjdk-8
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV TZ=Asia/Shanghai

## 时区
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## 字体
## RUN set -xe \ && apk --no-cache add ttf-dejavu fontconfig


#apt-get源 使用163的源
RUN mv /etc/apt/sources.list /etc/apt/sources.list.bak && \
    echo "deb http://mirrors.163.com/debian/ stretch main non-free contrib" >/etc/apt/sources.list && \
    echo "deb http://mirrors.163.com/debian/ stretch-updates main non-free contrib" >>/etc/apt/sources.list && \
    echo "deb http://mirrors.163.com/debian/ stretch-backports main non-free contrib" >>/etc/apt/sources.list && \
    echo "deb-src http://mirrors.163.com/debian/ stretch main non-free contrib" >>/etc/apt/sources.list && \
    echo "deb-src http://mirrors.163.com/debian/ stretch-updates main non-free contrib" >>/etc/apt/sources.list && \
    echo "deb-src http://mirrors.163.com/debian/ stretch-backports main non-free contrib" >>/etc/apt/sources.list && \
    echo "deb http://mirrors.163.com/debian-security/ stretch/updates main non-free contrib" >>/etc/apt/sources.list && \
    echo "deb-src http://mirrors.163.com/debian-security/ stretch/updates main non-free contrib" >>/etc/apt/sources.list

RUN CONFD_VERSION="0.16.0" && \
    CONFD_URL="https://github.com/kelseyhightower/confd/releases/download" && \
    apt-get update && \
    ## apt-get upgrade -y && \
    ## apt-get install -y locales bash openssl curl vim procps zip dnsutils net-tools iputils-ping socat tcpdump netcat && \
    apt-get install -y ttf-dejavu fontconfig && \
    curl -L --progress-bar ${CONFD_URL}/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64 -o /usr/bin/confd && \
    chmod +x /usr/bin/confd

## JVM工具
RUN mkdir /opt/vjtools && \
    VJTOOLS_VERSION="1.0.8" && \
    VJTOOLS_DUMP_URL="https://github.com/goDaryl/vjtools/blob/master/vjdump/vjdump.sh" && \
    VJTOOLS_MAVEN_URL="https://repo1.maven.org/maven2/com/vip/vjtools" &&\
    curl -L --progress-bar ${VJTOOLS_DUMP_URL} -o /opt/vjtools/vjdump.sh && \
    curl -L --progress-bar ${VJTOOLS_MAVEN_URL}/vjmap/${VJTOOLS_VERSION}/vjmap-${VJTOOLS_VERSION}.zip -o /tmp/vjmap.zip && \
    curl -L --progress-bar ${VJTOOLS_MAVEN_URL}/vjtop/${VJTOOLS_VERSION}/vjtop-${VJTOOLS_VERSION}.zip -o /tmp/vjtop.zip && \
    unzip /tmp/vjmap.zip -d /opt/vjtools/ && \
    unzip /tmp/vjtop.zip -d /opt/vjtools/ && \
    rm -rf /tmp/vjmap.zip /tmp/vjtop.zip && \
    chmod +x /opt/vjtools/vjdump.sh /opt/vjtools/vjmap/* /opt/vjtools/vjtop/* && \
    echo "alias vjdump='/opt/vjtools/vjdump.sh'\nalias vjmap='/opt/vjtools/vjmap/vjmap.sh'\nalias vjtop='/opt/vjtools/vjtop/vjtop.sh'\n" >> ~/.bashrc && \
    mkdir /opt/jmx-exporter && \
    JMX_EXPORTER_VERSION="0.12.0" && \
    JMX_EXPORTER_URL="https://repo1.maven.org/maven2/io/prometheus/jmx/jmx_prometheus_javaagent" && \
    curl -L --progress-bar ${JMX_EXPORTER_URL}/${JMX_EXPORTER_VERSION}/jmx_prometheus_javaagent-${JMX_EXPORTER_VERSION}.jar -o /opt/jmx-exporter/agent.jar && \
    JMXTERM_VERSION="1.0.0" && \
    JMXTERM_URL="https://github.com/jiaqi/jmxterm/releases/download" && \
    curl -L --progress-bar ${JMXTERM_URL}/v${JMXTERM_VERSION}/jmxterm-${JMXTERM_VERSION}.jar -o /opt/jmxterm.jar && \
    ARTHAS_URL="https://arthas.aliyun.com/arthas-boot.jar" && \
    curl -L --progress-bar ${ARTHAS_URL} -o /opt/arthas-boot.jar

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT [ "/entrypoint.sh" ]
