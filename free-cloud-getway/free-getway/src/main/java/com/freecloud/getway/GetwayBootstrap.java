package com.freecloud.getway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: maomao
 * @Date: 2019-08-23 13:39
 */
@SpringBootApplication
public class GetwayBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(GetwayBootstrap.class, args);
    }
}
