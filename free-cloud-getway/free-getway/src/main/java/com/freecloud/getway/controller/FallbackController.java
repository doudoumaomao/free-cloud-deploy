package com.freecloud.getway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: maomao
 * @Date: 2019-08-23 18:08
 */
@RestController
public class FallbackController {

    @GetMapping("/fallback")
    public Map<String,String> fallBackController() {
        Map<String, String> res = new HashMap();
        res.put("code", "-100");
        res.put("data", "service not available");
        return res;
    }
}
